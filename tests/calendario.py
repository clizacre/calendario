from datetime import datetime

calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    activities = []
    for date, times in calendar.items():
        for t, activity in times.items():
            if t == atime:
                activities.append((date, t, activity))
    return activities

def get_all():
    all_activities = []
    for date, times in calendar.items():
        for time, activity in times.items():
            all_activities.append((date, time, activity))
    return all_activities

def get_busiest():
    busiest_date = max(calendar, key=lambda k: len(calendar[k]))
    busiest_no = len(calendar[busiest_date])
    return (busiest_date, busiest_no)

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    try:
        datetime.strptime(date, "%Y-%m-%d")
        return True
    except ValueError:
        return False

def check_time(time):
    try:
        datetime.strptime(time, "%H:%M")
        return True
    except ValueError:
        return False

def get_activity():
    while True:
        date = input("Introduce la fecha (aaaa-mm-dd): ")
        if check_date(date):
            break
        else:
            print("Formato de fecha incorrecto. Inténtalo de nuevo.")

    while True:
        time = input("Introduce la hora (hh:mm): ")
        if check_time(time):
            break
        else:
            print("Formato de hora incorrecto. Inténtalo de nuevo.")

    activity = input("Introduce el nombre de la actividad: ")
    return (date, time, activity)

def menu():
    print("A. Introducir actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")

    while True:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            return option
        else:
            print("Opción no válida. Inténtalo de nuevo.")

def run_option(option):
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
        print("Actividad añadida correctamente.")
    elif option == 'B':
        activities = get_all()
        show(activities)
    elif option == 'C':
        busiest_date, busiest_no = get_busiest()
        print(f"El día más ocupado es {busiest_date} con {busiest_no} actividades.")
    elif option == 'D':
        atime = input("Introduce la hora (hh:mm): ")
        activities = get_time(atime)
        if activities:
            show(activities)
        else:
            print(f"No hay actividades a la hora {atime}.")
    elif option == 'X':
        print("¡Hasta luego!")

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
